# GitLab CI/CD

This guide describes how to setup and use GitLab CI/CD. 

Continuous Integration (CI) and Continuous Deployment (CD) are tools in GitLab that allows automated testing 
and deployment of code. For more information on it see the [official documentation](https://about.gitlab.com/product/continuous-integration/).  


## Table of Contents
* [Server Side Setup](#server-side-setup)
* [Getting Started](#getting-started)
* [Deploying Code](#deploying-code)
   * [Server Setup](#server-setup)
   * [Configuring the CI yaml](#configuring-the-ci-yaml)


## Server Side Setup
Continuous Integration (CI) is a tool in GitLab that allows automated testing and deployment of code. For more information on it 
see the [official documentation](https://about.gitlab.com/product/continuous-integration/).  

To set it up in our instance of GitLab, we require a shared runner server. A runner is where all the jobs run. 
You can have this setup on the same server as GitLab but then if there are any resource intensive job running it will bog 
down the GitLab service. 

So to start off, set up a new Ubuntu VM (we used 18.04 at the time this was written). Then we're ready to set it up as a runner. 

Follow the instructions here for installing the runner: https://docs.gitlab.com/runner/install/linux-manually.html

Once it is set up then you need to register it with our GitLab instance. 
from the runner server, perform the following steps: https://docs.gitlab.com/runner/register/index.html. To get the exact 
information needed to fill this out, go to Admin -> Runners and there will be a link to those same instructions as 
well as the variables to provide during the setup steps.

You can optionally set up different runners for different groups or for specific projects. You can include tags to differentiate 
them more easily. For example if one runner is built handle java code you could tag it `java`. The advantage of making 
different specialized runners is if one project needed special software installed on the runner to perform it's jobs, but you didn't want it available 
for all the projects for some reason, you could separate that out. For example if you didn't want just one runner server that handled all 
the jobs and had all the different software on it, it would make sense to split that out more.

Optionally, if you want one of your runners to be able to run jobs that have not specified a tag (for instance if you want it to just default 
to a basic bash runner) you need to configure it to allow it to do so. To do this, go to Admin -> Runners 
and click the pencil button by the runner to edit it. Then make sure the `Run untagged jobs` option is selected. Then save the changes. 

## Getting Started
This section is geared towards users and assumes the runner is already set up on your GitLab instance.

In your project, there will be a CI/CD option on the repository navigation bar. 
This contains all the pipelines (which are what is triggered from either a merge request or a commit) and the status of 
each job within the pipeline. What defines the jobs and their actions is in the `.gitlab-ci.yml` file of the repository. 
See the [full documentation](https://docs.gitlab.com/ee/ci/yaml/) for all the possibilities for setting it up and configuring it. 

As an example, if you had a repository that contained `index.html` you could have a `.gitlab-ci.yml` that contains:
```
stages:
 - first
 - second

before_script:
  - uptime
first_job:
  tags: 
    - bash
  stage: first
  script: xmllint index.html
second_job:
  tags: 
    - bash
  stage: second
  script: md5sum index.html

```
To describe what is happening here:
1. it will run the command `uptime` before running any of the jobs
2. It will run each stage listed in order, if one fails it will not continue
3. Each job must be associated with a stage (stages can have multiple jobs) and each job must define the command to be run 
(which is executed on the runner). So in this example it will first run `uptime` then `xmllint index.html` then `md5sum index.html` in that order.
The tag specifies which runner to use, in this case it will use the runner taged with `bash`

You can do a commit to trigger the pipeline and see the status of it from the pipelines page on the CI/CD menu. 

### Deploying Code
For a more advanced example, here is how you would use CI to deploy code to an environment. This requires a bit 
more pre-setup in order to work, but then the `.gitlab-ci.yaml` file is very straight forward.

The caveat with this example is that it requires you to have root access to the runner server as well as the 
server you wish to deploy to in order to do the server setup steps.

#### Server Setup

1. Create a new user on the server you wish to deploy code to that the runner server will be able to connect as.
From the server to deploy to:
```
adduser deploy
```

2. Add the user to the group that has ownership of the location where the project is checked out. 
Additionally, you need to lock the user from being able to login in. For example 
if a project is checked out to `/var/www/mysite` and is owned by `www-data:www-data` then you would do the 
following: 
```
adduser deploy www-data
passwd -l deploy
```

3. Make sure the group has write privileges on the folder the repository is cloned to:
```
cd /var/www/mysite
chgrp -R www-data .
chmod -R g+rw .
chmod g+ws `find . -type d` 
git init --bare --shared=all .
```

4. Make a deploy key so that the user is able to pull from the repository. Then make sure you enable 
the deploy key in your repository's settings.
```
sudo -Hu deploy ssh-keygen -o -t rsa -b 4096 -C "deploy@[deploy server]"
```

5. Create a key for the deploy user to connect to the deploy server from the runner server.
From the runner server:
```
ssh-keygen -o -t rsa -b 4096 -C "gitlab-runner@git-runner"
cat /home/gitlab-runner/.ssh/id_rsa.pub
```
On the deploy server, add the key to: `/home/deploy/.ssh/authorized_keys`
Test the connection by running the following from the runner server:
```
su gitlab-runner
ssh deploy@[deploy server]
```

6. If you need to run any additional commands on the remote server, make sure you enable your deploy user 
to be able to run them as sudo if the group level access they have is not enough. An example of this would 
be restarting Apache. Here is what you would add to the sudoers file for allowing Apache restarts:
```
# Allow deploy to restart apache
deploy ALL=(root) NOPASSWD: /bin/systemctl restart apache2, /usr/sbin/service apache2 restart
```

#### Configuring the CI yaml
Now that the permissions are all in place, we need to pull it all together in our `.gitlab-ci.yaml` file.

```
stages:
 - deploy

deploy_job:
  stage: deploy
  tags:
    - bash
  only: 
    - master
  script: 
    - ssh deploy@[deploy server] git -C /var/www/mysite pull origin master
    - ssh deploy@[deploy server] sudo systemctl restart apache2
```

* `only: master`: This means that the `deploy_job` job will only be triggered on commits/merges to the master 
branch. This was we can not have it deploying code or other actions if we are only touching a development 
branch. You could even split it further and have branches for each environment you want to deploy to.  
* `script`: The script section is each command you want to execute from the runner server, in order. If any of 
the steps fail, it will not continue. 
* `ssh deploy@[deploy server] git -C /var/www/mysite pull origin master`: Says to connect to the deploy server
ad the deploy user and pull the latest code from the master branch in the repository cloned to `/var/www/mysite`
* `ssh deploy@[deploy server] sudo systemctl restart apache2`: Will connect to the remote deploy server 
again and restart apache.
